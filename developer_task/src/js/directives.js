/**
 * Created by LuisCarlos on 22/02/2015.
 */
(function() {
    var app = angular.module('directives', []);

    app.directive("movieListItem", function() {
        return {
            restrict: 'E',
            templateUrl: "movie-list-item.html",
            controllerAs:"movieListItemCtrl",
            controller:function(){
                movieListItemCtrl = this;
                movieListItemCtrl.getImgSrc = function(imgId){
                    return urlImageThumbnailPoster(imgId);
                };
            }
        };
    });

    app.directive("tvListItem", function() {
        return {
            restrict: 'E',
            templateUrl: "tv-list-item.html",
            controllerAs:"tvListItemCtrl",
            controller:function(){
                tvListItemCtrl = this;
                tvListItemCtrl.getImgSrc = function(imgId){
                    return urlImageThumbnailPoster(imgId);
                };
            }
        };
    });

    app.directive("personListItem", function() {
        return {
            restrict: 'E',
            templateUrl: "person-list-item.html",
            controllerAs:"personListItemCtrl",
            controller:function(){
                personListItemCtrl = this;
                personListItemCtrl.getImgSrc = function(imgId){
                    return urlImageThumbnailPoster(imgId);
                };
            }
        };
    });
})();