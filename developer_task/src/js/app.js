/**
 * Created by LuisCarlos on 21/02/2015.
 */

var tmdbApi_key =  "e8e9d77d9724e4fec726fdf6f6943706";
var tmdbApi_url = "https://api.themoviedb.org/3/";
var tmdbApi_images_url = "https://image.tmdb.org/t/p/";

jQuery( document ).ready(function() {
    jQuery('#div-panel-results').hide();
});

(function() {
    var app = angular.module('tmdbApiUse', ['directives']);

    app.controller('SearchController', ['$http', function($http){
        var searchCtrl = this;

        searchCtrl.movieResult = {};
        searchCtrl.tvResult = {};
        searchCtrl.personResult = {};

        searchCtrl.doSearch = function(){
            $http.get(urlSearchQuery('movie', searchCtrl.query))
                .success(function(data, status, headers, config) {
                    searchCtrl.movieResult = data;
                })
                .error(function(data, status, headers, config) {
                    //ToDo: Handle Error
                    console.log("Error")
                });

            $http.get(urlSearchQuery('tv', searchCtrl.query))
                .success(function(data, status, headers, config) {
                    searchCtrl.tvResult = data;
                })
                .error(function(data, status, headers, config) {
                    //ToDo: Handle Error
                    console.log("Error")
                });

            $http.get(urlSearchQuery('person', searchCtrl.query))
                .success(function(data, status, headers, config) {
                    searchCtrl.personResult = data;
                })
                .error(function(data, status, headers, config) {
                    //ToDo: Handle Error
                    console.log("Error")
                });

            if (!jQuery('#div-panel-results').is(':visible')){
                jQuery('#div-panel-results').delay(500).show('slow');
            }
        };
    }]);
})();

var urlSearchQuery = function(by, query) {
    return    tmdbApi_url  + "search/" + by + "?"
            + "api_key=" + tmdbApi_key
            + "&"
            + "query=" + encodeURIComponent(query);
};

var urlImageThumbnailPoster =function(imgId){
    if(imgId != null && imgId != 'null'){
        return 'https://image.tmdb.org/t/p/w92' + imgId;
    }else{
        return 'https://d3a8mw37cqal2z.cloudfront.net/assets/e6497422f20fa74a240ad385ea4a2ade/images/no-poster-w92.jpg';
    }
};

